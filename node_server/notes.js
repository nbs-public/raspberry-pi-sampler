/**
 * We're using quite up-to-date syntax in this file, with the new ECMAScript 6 standard that isn't found in all web
 * browsers yet. The latest version of node is quite able to support all of the functions we're using here however.
 *
 * We're going to build this class up in much the same way as we did in python, with only a few differences due to
 * differing syntax between python and javascript.
 */
class NoteHandler {

    /**
     * The constructor function in a javascript class acts in the same way as the __init__ function in python, enabling
     * us to initialise the class with some variables.  In this case, we don't want to input anything, simply create the
     * notes property initialised to an object (which in javascript works similarly to a python dictionary)
     */
    constructor() {
        this.notes = {};
    }

    /**
     * We're doing things slightly differently again here, as Javascript doesn't have a key error.  Instead it returns
     * a special value to the variable called 'undefined'.  This acts as the boolean 'false' for the purposes of
     * checking it's existence, so we can verify whether the notes object exists or is undefined using the !notes check,
     * which will return true only if it doesn't exist, due to the 'not' flag.
     *
     * If the note does exist we can simply return it for our server to send back, but if it doesn't we will throw an
     * error that the server will detect and act upon, very similarly to the python code.
     */
    getNote(key) {
        let notes = this.notes[key];
        if (!notes) {
            throw ReferenceError;
        } else {
            return notes;
        }
    }

    /**
     * The add note function takes the functionality from the python module and the syntactical changes from above and
     * applies them to create a function that will check if the notes exist, and if not create an array with the value
     * in, ensuring something is always there to receive new notes.
     */
    addNote(key, value) {
        let existingNotes = this.notes[key];
        if (!existingNotes) {
            this.notes[key] = [value];
        } else {
            existingNotes.push(value);
            this.notes[key] = existingNotes;
        }
        return this.notes[key];
    }

}

/**
 * In Python we didn't have to export anything in order for other files to be able to see them, but in Javascript we
 * need to explicitly tell the file to expose the NoteHandler class for use by other files.  This allows you to include
 * things in these files that are not accessible to code outside, which can potentially be more secure.
 */
module.exports = NoteHandler;

/**
 * Finally we're going to add a quick runner that'll do a quick test of our class.
 * Unlike Python, this isn't standard practice in NodeJS and a testing library such as MochaJS will produce a lot more
 * consistency while testing.  However we can still do a quick visual check when running this notes.js file
 */
if (require.main === module) {
    let notesHandler = new NoteHandler();
    notesHandler.addNote('key', 'value');
    notesHandler.addNote('key', 'value2');
    notesHandler.addNote('key2', 'value3');
    console.log(`Value for 'key' is ${notesHandler.getNote('key')}`);
    console.log(`Value for 'key2' is ${notesHandler.getNote('key2')}`);
}
/**
 * We can run this file by using the node command, like:
 *      node notes.js
 * And we should see the output from above showing the class is working
 */
