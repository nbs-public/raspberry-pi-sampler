/**
 * We first bring in the modules we need access to using the require command.  Unlike in python where we used an "import"
 * command, we use the require function here.  This lets us specify a module or file we want to access, which modules in
 * the "node_modules" folder (aka installed via npm) can be accessed simply with a module name, specific files need a
 * relative path to them.
 */

const express = require('express');
const NoteHandler = require('./notes');

/**
 * Note that while we still don't use types for variables, we do need to declare them
 *      const declares the variable and ensures it cannot change throughout its lifespan
 *      let means you need to declare the variable only once per file, and it can be used in different scopes (preferred)
 *      var declares the variable only in the scope it's used in, meaning it may need to be declared more than once
 */
const bodyParser = require('body-parser');

/**
 * For this app we're going to be using a pair of libraries called "express" and "body-parser" for our server.
 *
 * Express is a server library that abstracts and enhances some of the default server library installed with NodeJS.
 * There are many libraries which server similar purposes, but express is one of the most commonly used due to it's
 * simplicity and speed.
 *
 * Body-parser is a library that used to be included in express, but was moved out of it to allow for greater
 * customisation.  It provides a set of tools that will transform encoded bytes from a request into something readable
 * by our code.
 */
let app = express();

/**
 * Unlike in python, to initialise the noteHandler object we need to use the new keyword, which is a common requirement
 * across C-like languages.
 */
let noteHandler = new NoteHandler();

/**
 * Whereas in our python server the flask module handled all of the inputs through the request object, in express we
 * need to directly give the express server a parser to convert the request into a format we can use.
 * The only input we're expecting is JSON as we are only sending JSON from our frontend, so the only parser we need to
 * register with our app is the JSON parser.
 * We don't need to explicitly set a path on this app.use function as we want
 * all requests coming into our server to pass through it.
 */
app.use(bodyParser.json());

/**
 * Instead of explicitly rendering a file like with the flask server, with express we can simply expose a folder on an
 * endpoint.  This will serve up any specifically requested files in the request, or simply pass back the index.html
 * document found within that folder.
 */
app.get('/', express.static('public'));

/**
 * Our /notes function is instead split into two functions in express, as it enables us to explicitly define one
 * function for each endpoint and method on that endpoint, and in general being explicit is better than being implicit.
 * Otherwise it functions much the same way as in python, we extract a key from our request object, try to retrieve any
 * notes saved with that key, and if there are errors we handle them and send back the same responses as from before.
 *
 * The main difference is in how we send back data.  In express we get a response object, or 'res' that allows us to
 * do more configuration on the response than we could in flask.  In this case, we don't actually want to do much, and
 * can just send an explicit http status code (using .status), a string of data (using .send) or convert and send a JSON
 * object back to the front-end (using .json).
 */
app.get('/notes', (req, res) => {
    let key = req.param('name');
    try {
        let notes = noteHandler.getNote(key);
        res.json(notes);
    } catch (error) {
        if (error instanceof ReferenceError) {
            res.status(400).send('Key could not be found');
        } else {
            console.log(error);
            res.status(500).send('Internal server error occurred');
        }
    }
});

/**
 * Similarly to above, the POST part of the /notes endpoint is found in a separate function.  In this case we're
 * extracting the keys and values from the body of the request, which has been processed by our JSON parser above.
 * We then attempt to enter the key and value into the noteHandler, and catch the error if one occurs.
 */
app.post('/notes', (req, res) => {
    let key = req.body['name'],
        value = req.body['note'];
    try {
        let notes = noteHandler.addNote(key, value);
        res.json(notes);
    } catch (error) {
        console.log(error);
        res.status(500).send('Internal server error occurred');
    }
});

/**
 * Finally, we need to tell the app to start listening.  This means it'll keep the process alive and start responding to
 * HTTP traffic on port specified, meaning we can test our application!
 */
app.listen(3000, () => {
    console.log('Server listening on port 3000')
});
