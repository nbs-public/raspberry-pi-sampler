from flask import Flask, request, render_template
from notes import NotesHandler
import sys
import json

app = Flask(__name__)

noteHandler = NotesHandler()

@app.route("/", methods=["GET"])
def index():
    return render_template('index.html') 

@app.route("/notes", methods=["GET", "POST"])
def getNotes():
    if request.method == "GET":
        try:
            name = request.args.get("name")
            note = noteHandler.getNote(name)
            return json.dumps(note)
        except KeyError:
            return "Key could not be found", 404
        except:
            print "getErr:" + sys.exc_info()
    elif request.method == "POST":
        try:
           print request.form
           input = json.loads(request.data)
           noteHandler.addNote(input["name"], input["note"])
           return json.dumps(noteHandler.getNote(input["name"]))
        except:
           print "psotErr:" + sys.exc_info()

if (__name__) == ("__main__"):
    app.run(host="0.0.0.0", port=3000)
