class NotesHandler:
    def __init__(self):
        self.notes = {}

    def addNote(self, key, value):
        print "Adding key " + key + " to notes with value " + value
        try:
            currentNotes = self.notes[key]
            currentNotes.append(value)
            self.notes[key] = currentNotes
        except KeyError:
            self.notes[key] = [value]

    def getNote(self, key):
        print "Getting value for key " + key
        return self.notes[key]

if __name__ == "__main__":
    notesHandler = NotesHandler()
    notesHandler.addNote("key1", "val1")
    notesHandler.addNote("key2", "val2")
    print notesHandler.getNote("key1")
    print notesHandler.getNote("key2")
    
